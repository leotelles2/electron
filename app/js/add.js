const { ipcRenderer } = require("electron");

let pessoa = document.querySelector("#pessoa")
let cadastro = document.querySelector("#cad")
let loja = document.querySelector("#loja")

pessoa.addEventListener('click', () =>{
    cadastro.innerHTML=""

    let formulario= 
    '<form class="formulario">'+
    '   <h3>Cadastro de Usuário</h3>'+
    '   <div class="form-control">'+
    '       <label for="cNome">Nome:</label><br>'+
    '       <input required type="text" name="tNome" id="cNome" size="20" maxlength="30" placeholder="Nome Completo">'+
    '   </div>'+
    '   <div class="form-control">'+
    '        <label for="cTel">Telefone</label>'+
    '       <input required pattern="^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,7}$" type="tel" name="tTel" id="cTel" size="20" maxlength="13" placeholder="Ex:(00)00000.0000" >'+
    '   </div>'+
    '   <div class="form-control">'+
    '       <label for="cCpf">Cpf:</label><br>'+
    '      <input type="text" name="cCpf" id="cCom" size="13" maxlength="80" placeholder="000.000.000-00">'+
    '   </div>'+
    '   <div class="form-control">'+
    '       <label for="cSenha">Senha:</label> <br>'+
    '       <input  pattern="^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$ %^&*-_]).{8,}$" type="password" name="tSenha" id="cSenha" size="8" placeholder="8 Dígitos"><br>'+
    '       <small>Minimo: 1 letra maiúscula, 8 caracteres , 1 número, 1 caractere especial</small>'+
    '   </div>'+
    '   <div class="form-control">'+
    '       <label for="cConf"> Confirmar Senha:</label>'+
    '       <input required pattern="^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$ %^&*-_]).{8,}$" type="password" name="tConf" id="cConf" size="8" placeholder="8 Dígitos">'+
    '   </div>'+
    '   <div class="form-control">'+
    '       <button><a href="../html/index.html" class="redirecionamento">Voltar</a> </button>'+
    '       <button id="cadastro">Cadastro</button>'+
    '   </div>'+
    '</form>'

    cadastro.innerHTML =  formulario

    teste()
})

loja.addEventListener('click', () =>{
    console.log("muahahahah")
    cadastro.innerHTML=""

   let formulario = 
        '<form class="formulario">'+
        '   <h3>Cadastro Loja</h3>'+
        '   <div class="form-control">'+
        '       <label for="cNome"> Proprietário:</label><br>'+
        '       <input  pattern="" type="text" name="tNome" id="cNome" size="10" maxlength="50" placeholder="Nome Completo">'+
        '   </div>'+
        '   <div class="form-control">'+
        '       <label for="cNomeLoja"> Nome da Loja:</label><br>'+
        '       <input  pattern="" type="text" name="tNomeLoja" id="cNomeLoja" placeholder="Nome Completo">'+
        '   </div>'+
        '   <div class="form-control">'+
        '        <label for="cTel">Telefone</label>'+
        '       <input required pattern="^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,7}$" type="tel" name="tTel" id="cTel" size="20" maxlength="13" placeholder="Ex:(00)00000.0000" >'+
        '   </div>'+
        '   <div class="form-control">'+
        '       <label for="cCnpj">Cnpj</label><br>'+
        '      <input required pattern="//^\d{2}\.\d{3}\.\d{3}\/\d{4}\-\d{2}$//" type="Cnpj" name="tCnpj" id="cCnpj" size="20" maxlength="14" placeholder="Ex: XX. XXX. XXX/XXXX-XX">'+
        '   </div>'+
        '   <div class="form-control">'+
        '       <label for="cSenha">Senha:</label> <br>'+
        '       <input  type="password" name="tSenha" id="cSenha" size="8" placeholder="8 Dígitos"><br>'+
        '       <small>Minimo: 1 letra maiúscula, 8 caracteres , 1 número, 1 caractere especial</small>'+
        '   </div>'+
        '   <div class="form-control">'+
        '       <label for="cConf"> Confirmar Senha:</label>'+
        '       <input required type="password" name="tConf" id="cConf" size="8" placeholder="8 Dígitos">'+
        '   </div>'+
        '   <div class="form-control">'+
        '       <button><a href="../html/index.html" class="redirecionamento">Voltar</a> </button>'+
        '       <button id="cadastro">Cadastro</button>'+
        '   </div>'+
        '</form>'

    cadastro.innerHTML =  formulario

    teste()
})

function teste(){
    let cadastro_button = document.querySelector("#cadastro")



    cadastro_button.addEventListener('click', () => {
        let dados
        if (document.querySelector("#cNomeLoja") !== null ) {
            console.log("cadastro de lojas")

            dados= [{
                nome: document.querySelector("#cNome").value,
                nomeLoja: document.querySelector("#cNomeLoja").value,
                telefone: document.querySelector("#cTel").value,
                cnpj: document.querySelector("#cCnpj").value,
                senha: document.querySelector("#cSenha").value
            }]
            
        }else{
            console.log("cadastro de usuario")

            dados =[{
                nome: document.querySelector("#cNome").value,
                telefone: document.querySelector("#cTel").value,
                cpf: document.querySelector("#cCom").value,
                senha: document.querySelector("#cSenha").value
            }]
            
        }
        ipcRenderer.send("envio-de-dados", dados)
    })
}
ipcRenderer.on("cadastro", (event, arg) => {
    console.log(arg)
})