const { ipcRenderer } = require("electron");

let button_login = document.querySelector("#login");
let button_cad_pessoa = document.querySelector("#cad_pessoa");
let link_html = document.querySelector("#linkao");

link_html.addEventListener("click", () => {
    ipcRenderer.send("abrir-janela-sobre")
})

button_cad_pessoa.addEventListener('click', () =>{
    window.location.href = "../html/cadastro.html"
})

button_login.addEventListener("click", () => {
    ipcRenderer.send("verificar_login", {nome: document.querySelector("#nome").value,senha: document.querySelector("#cSenha").value})
})

ipcRenderer.on("valid?", (event,arg) => {
    if(arg){
        window.location.href = "../html/principal.html"
    }else{
        alert("O login está errado")
    }
})

