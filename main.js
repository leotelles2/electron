const { app ,BrowserWindow, ipcMain} = require("electron");

const array =  [{
    nome: "leonardo", senha:"leotf"
},{
    nome: "teste", senha:"teste"
},{
    nome: "senha", senha:"senha"
},{
    nome: "123123", senha:"123123"
},{
    nome: "teste123", senha:"teste123"
}]


app.on('ready', () => {
    console.log("janela janela")
    let main_window = new BrowserWindow({
        height: 800,
        width: 1360,
        webPreferences: {
            nodeIntegration: true
        }
    })

    console.log(__dirname)
    main_window.loadURL(`file://${__dirname}/app/html/index.html`)
});

app.on('window-all-closed', () => {
    app.quit()
})

let sobre_window = null
ipcMain.on("abrir-janela-sobre", () => {
    if(sobre_window == null){
        sobre_window = new BrowserWindow({
            height: 370,
            width: 400,
            alwaysOnTop: true,
            frame: false,
            webPreferences: {
                nodeIntegration: true
            }

        })
    }

    sobre_window.on("closed", () => {
        sobre_window = null
    })

    sobre_window.loadURL(`file://${__dirname}/app/html/sobre.html`)
})


ipcMain.on("fechar-janela-sobre", () => {
    sobre_window.close()
})

ipcMain.on("verificar_login", (event,arg) => {
    //aqui vcs tem que colocar uma query no banco verificando se os campos que estãoa aqui são os mesmos que tem no banco 
    console.log(arg)


    let valid = false
    for(var i = 0; i < array.length; i++){
        if(array[i].nome == arg.nome && array[i].senha == arg.senha){
            console.log(i)
            valid = true
        }        
    }

    event.reply("valid?", valid)


})

ipcMain.on("envio-de-dados", (event,arg) => {
    // aqui vcs tem que inserir no banco de dados as informações do cadastrante
    console.log(arg)
    cad = true
    event.reply("cadastro", cad)

})